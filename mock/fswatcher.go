// Code generated by mockery v2.35.4. DO NOT EDIT.

package mock

import (
	pkg "go-data-backup/internal/pkg"

	mock "github.com/stretchr/testify/mock"
)

// FsWatcher is an autogenerated mock type for the FsWatcher type
type FsWatcher struct {
	mock.Mock
}

// Add provides a mock function with given fields: path
func (_m *FsWatcher) Add(path string) error {
	ret := _m.Called(path)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(path)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Events provides a mock function with given fields:
func (_m *FsWatcher) Events() <-chan pkg.FsEvent {
	ret := _m.Called()

	var r0 <-chan pkg.FsEvent
	if rf, ok := ret.Get(0).(func() <-chan pkg.FsEvent); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(<-chan pkg.FsEvent)
		}
	}

	return r0
}

// NewFsWatcher creates a new instance of FsWatcher. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewFsWatcher(t interface {
	mock.TestingT
	Cleanup(func())
}) *FsWatcher {
	mock := &FsWatcher{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
