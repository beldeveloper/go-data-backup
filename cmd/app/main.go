package main

import (
	"os"
	"os/signal"
	"syscall"

	"go-data-backup/internal/app"
	"go-data-backup/internal/app/job"
	"go-data-backup/internal/pkg"
	"go-data-backup/internal/pkg/fsnotify"
	"go-data-backup/internal/pkg/noop"
	ospkg "go-data-backup/internal/pkg/os"
	"go-data-backup/internal/pkg/rsync"
	"go-data-backup/internal/pkg/telegram"
)

func main() {
	cfg := loadConfig()

	fs := ospkg.NewFilesystem(cfg.Backup.BackupPath)
	syncer := rsync.NewSyncer()
	notify := getNotificationService(cfg)
	watcher := fsnotify.NewWatcher(notify)
	events := job.NewEventManager(watcher.Events())
	backup := job.NewBackup(cfg.Backup, fs, watcher, syncer, notify, events.Events())

	if err := backup.InitFsWatcher(); err != nil {
		notify.PushErr(err)
		return
	}

	go watcher.Watch()
	go events.Watch()
	go backup.Watch()

	notify.PushInfoText("Backup service is started")

	interruptCh := make(chan os.Signal, 1)
	signal.Notify(interruptCh, os.Interrupt, syscall.SIGTERM)
	<-interruptCh

	backup.Close()
	events.Close()
	watcher.Close()

	notify.PushInfoText("Backup service is stopped")
	notify.Stop()
}

func getNotificationService(cfg app.Config) pkg.NotificationService {
	switch cfg.Notifications.Adapter {
	case "telegram":
		service := telegram.NewNotificationService(
			cfg.Notifications.Telegram.BotApiToken,
			cfg.Notifications.Telegram.ChatID,
		)
		go service.RunJob()
		return service
	default:
		return noop.NewNotificationService()
	}
}
