package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"go-data-backup/internal/app"

	"gopkg.in/yaml.v3"
)

func loadConfig() app.Config {
	cfgData, err := os.ReadFile(os.Getenv("APP_CONFIG"))
	if err != nil {
		log.Fatalln(err)
	}

	var cfg app.Config
	err = yaml.Unmarshal(cfgData, &cfg)
	if err != nil {
		log.Fatalln(err)
	}

	return validateConfig(cfg)
}

func validateConfig(cfg app.Config) app.Config {
	errors := make([]string, 0)
	var errStr string

	for i, path := range cfg.Backup.WatchPaths {
		cfg.Backup.WatchPaths[i], errStr = processPath(fmt.Sprintf("watch path %d", i), path)
		if errStr != "" {
			errors = append(errors, errStr)
		}
	}

	cfg.Backup.BackupPath, errStr = processPath("backup path", cfg.Backup.BackupPath)
	if errStr != "" {
		errors = append(errors, errStr)
	}

	cfg.Backup.StripPathPrefix, _ = processPath("strip path prefix", cfg.Backup.StripPathPrefix)

	switch cfg.Notifications.Adapter {
	case "telegram":
		if cfg.Notifications.Telegram.BotApiToken == "" {
			errors = append(errors, "telegram bot API token is required")
		}

		if cfg.Notifications.Telegram.ChatID <= 0 {
			errors = append(errors, "telegram chat ID is required")
		}
	}

	if len(errors) > 0 {
		for _, e := range errors {
			log.Println(e)
		}
		os.Exit(1)
	}

	return cfg
}

func processPath(name, path string) (string, string) {
	path = strings.TrimSpace(path)
	path = strings.TrimRight(path, "/")

	if path == "" {
		return "", fmt.Sprintf("%s is required", name)
	}

	return path, ""
}
