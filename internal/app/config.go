package app

type Config struct {
	Backup        BackupConfig `yaml:"backup"`
	Notifications struct {
		Adapter  string `yaml:"adapter"`
		Telegram struct {
			BotApiToken string `yaml:"botApiToken"`
			ChatID      int64  `yaml:"chatId"`
		} `yaml:"telegram"`
	} `yaml:"notifications"`
}

type BackupConfig struct {
	WatchPaths      []string `yaml:"watchPaths"`
	StripPathPrefix string   `yaml:"stripPathPrefix"`
	BackupPath      string   `yaml:"backupPath"`
}
