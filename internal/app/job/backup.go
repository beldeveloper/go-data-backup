package job

import (
	"os"
	"path/filepath"
	"strings"

	"go-data-backup/internal/app"
	"go-data-backup/internal/pkg"
	"go-data-backup/internal/pkg/errors"
)

func NewBackup(
	cfg app.BackupConfig,
	fs pkg.Filesystem,
	watcher pkg.FsWatcher,
	syncer pkg.Syncer,
	notify pkg.NotificationService,
	events <-chan pkg.FsEvent,
) Backup {
	return Backup{
		cfg:     cfg,
		fs:      fs,
		watcher: watcher,
		syncer:  syncer,
		notify:  notify,
		events:  events,
		closeCh: make(chan int),
	}
}

type Backup struct {
	cfg     app.BackupConfig
	fs      pkg.Filesystem
	watcher pkg.FsWatcher
	syncer  pkg.Syncer
	notify  pkg.NotificationService
	events  <-chan pkg.FsEvent
	closeCh chan int
	doneCh  chan pkg.FsEvent
}

func (j Backup) InitFsWatcher() error {
	for _, path := range j.cfg.WatchPaths {
		isDir, err := j.fs.IsDir(path)
		if err != nil {
			return err
		}

		if isDir {
			err = j.watchDir(path)
		} else {
			err = j.watcher.Add(path)
		}
		if err != nil {
			return err
		}
	}

	return nil
}

func (j Backup) watchDir(path string) error {
	entries, err := j.fs.ReadDir(path)
	if err != nil {
		return err
	}

	err = j.watcher.Add(path)
	if err != nil {
		return err
	}

	for _, entry := range entries {
		if !entry.IsDir {
			continue
		}

		err = j.watchDir(path + "/" + entry.Name)
		if err != nil {
			return err
		}
	}

	return nil
}

func (j Backup) Watch() {
	for {
		select {
		case event := <-j.events:
			j.consumeEvent(event)
			if j.doneCh != nil {
				j.doneCh <- event
			}
		case <-j.closeCh:
			return
		}
	}
}

func (j Backup) consumeEvent(event pkg.FsEvent) {
	if !j.checkBackupDir() {
		j.notify.PushWarningText("Backup dir isn't found. Got event: dir=%s; op=%d", event.Path, event.Op)
		return
	}

	switch event.Op {
	case pkg.FsEventCreate, pkg.FsEventModify:
		j.backupFile(event.Path)
	case pkg.FsEventRename, pkg.FsEventRemove:
		j.removeFile(event.Path)
	}
}

func (j Backup) checkBackupDir() bool {
	isDir, err := j.fs.IsDir(j.cfg.BackupPath)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			j.notify.PushErr(err)
		}
		return false
	}
	if !isDir {
		j.notify.PushErrorText("Is backup dir a file? O_o")
		return false
	}
	return true
}

func (j Backup) backupFile(path string) {
	isDir, err := j.fs.IsDir(path)
	if err != nil {
		j.notify.PushErr(err)
		return
	}

	if isDir {
		err = j.watchDir(path)
		if err != nil {
			j.notify.PushErr(err)
		}
	}

	err = j.sync(path, isDir)
	if err != nil {
		j.notify.PushErr(err)
	}
}

func (j Backup) sync(src string, isDir bool) error {
	dst := j.convToBackupPath(src)

	if !isDir {
		dst = filepath.Dir(dst)
	}

	err := j.fs.Mkdir(dst)
	if err != nil {
		return err
	}

	err = j.syncer.Sync(src, dst, isDir)
	if err != nil {
		return err
	}

	j.notify.PushInfoText("Synced: %s -> %s", src, dst)
	return nil
}

func (j Backup) removeFile(path string) {
	path = j.convToBackupPath(path)
	err := j.fs.Remove(path)
	if err != nil {
		j.notify.PushErr(err)
		return
	}

	j.notify.PushInfoText("Deleted: %s", path)
}

func (j Backup) convToBackupPath(path string) string {
	return j.cfg.BackupPath + strings.TrimPrefix(path, j.cfg.StripPathPrefix)
}

func (j Backup) Close() {
	close(j.closeCh)
}
