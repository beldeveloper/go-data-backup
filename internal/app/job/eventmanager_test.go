package job

import (
	"testing"
	"time"

	"go-data-backup/internal/pkg"

	"github.com/stretchr/testify/assert"
)

func Test_EventManager(t *testing.T) {
	inEvents := []pkg.FsEvent{
		// create /tmp/new/a/[1|2].txt
		{
			Path: "/tmp/new/a/1.txt",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/a/2.txt",
			Op:   pkg.FsEventModify,
		},
		// create /tmp/new/b/*
		{
			Path: "/tmp/new/b/1.txt",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/b/2.txt",
			Op:   pkg.FsEventModify,
		},
		{
			Path: "/tmp/new/b/foo/3.txt",
			Op:   pkg.FsEventModify,
		},
		{
			Path: "/tmp/new/b",
			Op:   pkg.FsEventCreate,
		},
		// create /tmp/new/c/*
		{
			Path: "/tmp/new/c",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/c/1.txt",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/c/2.txt",
			Op:   pkg.FsEventModify,
		},
		{
			Path: "/tmp/new/c/foo/3.txt",
			Op:   pkg.FsEventCreate,
		},
		// create /tmp/new/d/1.txt
		{
			Path: "/tmp/new/d/1.txt",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/d/1.txt",
			Op:   pkg.FsEventCreate,
		},
		// create and remove /tmp/new/e/1.txt
		{
			Path: "/tmp/new/e/1.txt",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/e/1.txt",
			Op:   pkg.FsEventRemove,
		},
		// create /tmp/new/f and remove /tmp/new/f/1.txt
		{
			Path: "/tmp/new/f",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/f/1.txt",
			Op:   pkg.FsEventRemove,
		},
		// remove /tmp/new/g and create /tmp/new/g/1.txt
		{
			Path: "/tmp/new/g",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/new/g/1.txt",
			Op:   pkg.FsEventCreate,
		},
		// remove /tmp/new/h/q.txt and create /tmp/new/h/1.txt
		{
			Path: "/tmp/new/h/1.txt",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/new/h/1.txt",
			Op:   pkg.FsEventCreate,
		},
		// delete /tmp/old/a/[1|2].txt
		{
			Path: "/tmp/old/a/1.txt",
			Op:   pkg.FsEventRename,
		},
		{
			Path: "/tmp/old/a/2.txt",
			Op:   pkg.FsEventRemove,
		},
		// delete /tmp/old/b/*
		{
			Path: "/tmp/old/b/1.txt",
			Op:   pkg.FsEventRename,
		},
		{
			Path: "/tmp/old/b/2.txt",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/old/b/foo/3.txt",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/old/b",
			Op:   pkg.FsEventRemove,
		},
		// delete /tmp/old/c/*
		{
			Path: "/tmp/old/c",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/old/c/1.txt",
			Op:   pkg.FsEventRename,
		},
		{
			Path: "/tmp/old/c/2.txt",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/old/c/foo/3.txt",
			Op:   pkg.FsEventRemove,
		},
		// delete /tmp/old/d/1.txt
		{
			Path: "/tmp/old/d/1.txt",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/old/d/1.txt",
			Op:   pkg.FsEventRemove,
		},
	}

	expEvents := []pkg.FsEvent{
		// create /tmp/new/a/[1|2].txt
		{
			Path: "/tmp/new/a/1.txt",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/a/2.txt",
			Op:   pkg.FsEventModify,
		},
		// create /tmp/new/b/*
		{
			Path: "/tmp/new/b",
			Op:   pkg.FsEventCreate,
		},
		// create /tmp/new/c/*
		{
			Path: "/tmp/new/c",
			Op:   pkg.FsEventCreate,
		},
		// create /tmp/new/d/1.txt
		{
			Path: "/tmp/new/d/1.txt",
			Op:   pkg.FsEventCreate,
		},
		// create and remove /tmp/new/e/1.txt
		// nothing

		// create /tmp/new/f and remove /tmp/new/f/1.txt
		{
			Path: "/tmp/new/f",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/tmp/new/f/1.txt",
			Op:   pkg.FsEventRemove,
		},
		// remove /tmp/new/g and create /tmp/new/g/1.txt
		{
			Path: "/tmp/new/g",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/new/g/1.txt",
			Op:   pkg.FsEventCreate,
		},
		// remove /tmp/new/h/q.txt and create /tmp/new/h/1.txt
		{
			Path: "/tmp/new/h/1.txt",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/tmp/new/h/1.txt",
			Op:   pkg.FsEventCreate,
		},
		// delete /tmp/old/a/[1|2].txt
		{
			Path: "/tmp/old/a/1.txt",
			Op:   pkg.FsEventRename,
		},
		{
			Path: "/tmp/old/a/2.txt",
			Op:   pkg.FsEventRemove,
		},
		// delete /tmp/old/b/*
		{
			Path: "/tmp/old/b",
			Op:   pkg.FsEventRemove,
		},
		// delete /tmp/old/c/*
		{
			Path: "/tmp/old/c",
			Op:   pkg.FsEventRemove,
		},
		// delete /tmp/old/d/1.txt
		{
			Path: "/tmp/old/d/1.txt",
			Op:   pkg.FsEventRemove,
		},
	}

	inEventsCh := make(chan pkg.FsEvent)
	manager := NewEventManager(inEventsCh)
	go manager.Watch()

	for _, e := range inEvents {
		inEventsCh <- e
	}

	outEvents := make([]pkg.FsEvent, 0, len(expEvents))
	go func() {
		for {
			outEvents = append(outEvents, <-manager.Events())
		}
	}()

	time.Sleep(freezeDuration + time.Second)

	manager.Close()

	assert.Equal(t, expEvents, outEvents)
}
