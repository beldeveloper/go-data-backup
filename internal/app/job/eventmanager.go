package job

import (
	"strings"
	"sync"
	"time"

	"go-data-backup/internal/pkg"
)

const (
	freezeDuration = time.Second
)

func NewEventManager(events <-chan pkg.FsEvent) *EventManager {
	return &EventManager{
		inEventsCh:  events,
		outEventsCh: make(chan pkg.FsEvent),
		closeCh:     make(chan int),
	}
}

type EventManager struct {
	inEventsCh  <-chan pkg.FsEvent
	outEventsCh chan pkg.FsEvent
	closeCh     chan int

	mux         sync.Mutex
	queue       []pkg.FsEvent
	freezeUntil time.Time
	releasing   bool
}

func (m *EventManager) Events() <-chan pkg.FsEvent {
	return m.outEventsCh
}

func (m *EventManager) Watch() {
	for {
		select {
		case event := <-m.inEventsCh:
			m.enqueueEvent(event)
		case <-m.closeCh:
			return
		}
	}
}

func (m *EventManager) enqueueEvent(event pkg.FsEvent) {
	m.mux.Lock()
	defer m.mux.Unlock()

	var enqueued bool
	switch event.Op {
	case pkg.FsEventCreate, pkg.FsEventModify:
		enqueued = m.processEventModify(event)
	case pkg.FsEventRename, pkg.FsEventRemove:
		enqueued = m.processEventRemove(event)
	}

	m.freezeUntil = time.Now().Add(freezeDuration)

	if enqueued && !m.releasing {
		m.releasing = true
		go m.releaseEvents()
	}
}

func (m *EventManager) processEventModify(event pkg.FsEvent) bool {
	for i := len(m.queue) - 1; i >= 0; i-- {
		q := m.queue[i]

		if (q.Op == pkg.FsEventRename || q.Op == pkg.FsEventRemove) &&
			(q.Path == event.Path || strings.HasPrefix(event.Path, q.Path+"/")) {
			break
		}

		if (q.Op == pkg.FsEventCreate || q.Op == pkg.FsEventModify) &&
			(q.Path == event.Path || strings.HasPrefix(event.Path, q.Path+"/")) {
			return false
		}

		if (q.Op == pkg.FsEventCreate || q.Op == pkg.FsEventModify) &&
			(q.Path == event.Path || strings.HasPrefix(q.Path, event.Path+"/")) {
			m.queue = append(m.queue[:i], m.queue[i+1:]...)
		}
	}

	m.queue = append(m.queue, event)
	return true
}

func (m *EventManager) processEventRemove(event pkg.FsEvent) bool {
	for i := len(m.queue) - 1; i >= 0; i-- {
		q := m.queue[i]

		if (q.Op == pkg.FsEventCreate || q.Op == pkg.FsEventModify) &&
			(q.Path == event.Path || strings.HasPrefix(event.Path, q.Path+"/")) {
			if q.Path == event.Path {
				m.queue = append(m.queue[:i], m.queue[i+1:]...)
				return false
			}
			if strings.HasPrefix(event.Path, q.Path+"/") {
				break
			}
		}

		if (q.Op == pkg.FsEventRename || q.Op == pkg.FsEventRemove) &&
			(q.Path == event.Path || strings.HasPrefix(event.Path, q.Path+"/")) {
			return false
		}

		if (q.Op == pkg.FsEventRename || q.Op == pkg.FsEventRemove) &&
			strings.HasPrefix(q.Path, event.Path+"/") {
			m.queue = append(m.queue[:i], m.queue[i+1:]...)
		}
	}

	m.queue = append(m.queue, event)
	return true
}

func (m *EventManager) releaseEvents() {
	for {
		if time.Now().Before(m.freezeUntil) {
			time.Sleep(freezeDuration)
			continue
		}

		if !m.releaseEvent() {
			return
		}
	}
}

func (m *EventManager) releaseEvent() bool {
	m.mux.Lock()

	if len(m.queue) == 0 {
		m.releasing = false
		m.mux.Unlock()
		return false
	}

	event := m.queue[0]
	m.queue = m.queue[1:]
	m.mux.Unlock()

	m.outEventsCh <- event
	return true
}

func (m *EventManager) Close() {
	close(m.closeCh)
}
