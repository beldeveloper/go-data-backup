package job

import (
	"errors"
	"os"
	"testing"

	"go-data-backup/internal/app"
	"go-data-backup/internal/pkg"
	"go-data-backup/mock"

	"github.com/stretchr/testify/assert"
)

func Test_Backup_InitFsWatcher_returns_is_dir_error(t *testing.T) {
	cfg := app.BackupConfig{
		WatchPaths: []string{
			"/home/user/Music",
		},
	}

	expErr := errors.New("test error")
	fs := mock.NewFilesystem(t)
	fs.On("IsDir", "/home/user/Music").Return(false, expErr).Once()

	backup := NewBackup(cfg, fs, nil, nil, nil, nil)
	err := backup.InitFsWatcher()

	assert.ErrorIs(t, err, expErr)
}

func Test_Backup_InitFsWatcher_returns_add_file_error(t *testing.T) {
	cfg := app.BackupConfig{
		WatchPaths: []string{
			"/home/user/.gitconfig",
		},
	}

	fs := mock.NewFilesystem(t)
	fs.On("IsDir", "/home/user/.gitconfig").Return(false, nil).Once()

	expErr := errors.New("test error")
	watcher := mock.NewFsWatcher(t)
	watcher.On("Add", "/home/user/.gitconfig").Return(expErr).Once()

	backup := NewBackup(cfg, fs, watcher, nil, nil, nil)
	err := backup.InitFsWatcher()

	assert.ErrorIs(t, err, expErr)
}

func Test_Backup_InitFsWatcher_add_dir_returns_read_dir_error(t *testing.T) {
	cfg := app.BackupConfig{
		WatchPaths: []string{
			"/home/user/Music",
		},
	}

	expErr := errors.New("test error")
	fs := mock.NewFilesystem(t)
	fs.On("IsDir", "/home/user/Music").Return(true, nil).Once()
	fs.On("ReadDir", "/home/user/Music").Return(nil, expErr).Once()

	backup := NewBackup(cfg, fs, nil, nil, nil, nil)
	err := backup.InitFsWatcher()

	assert.ErrorIs(t, err, expErr)
}

func Test_Backup_InitFsWatcher_add_dir_returns_add_error(t *testing.T) {
	cfg := app.BackupConfig{
		WatchPaths: []string{
			"/home/user/Music",
		},
	}

	fs := mock.NewFilesystem(t)
	fs.On("IsDir", "/home/user/Music").Return(true, nil).Once()
	fs.On("ReadDir", "/home/user/Music").Return(nil, nil).Once()

	expErr := errors.New("test error")
	watcher := mock.NewFsWatcher(t)
	watcher.On("Add", "/home/user/Music").Return(expErr).Once()

	backup := NewBackup(cfg, fs, watcher, nil, nil, nil)
	err := backup.InitFsWatcher()

	assert.ErrorIs(t, err, expErr)
}

func Test_Backup_InitFsWatcher_watch_nested_dir_returns_error(t *testing.T) {
	cfg := app.BackupConfig{
		WatchPaths: []string{
			"/home/user/Music",
		},
	}

	expErr := errors.New("test error")
	fs := mock.NewFilesystem(t)
	fs.On("IsDir", "/home/user/Music").Return(true, nil).Once()
	fs.On("ReadDir", "/home/user/Music").Return([]pkg.DirEntry{{IsDir: true, Name: "Queen"}}, nil).Once()
	fs.On("ReadDir", "/home/user/Music/Queen").Return(nil, expErr).Once()

	watcher := mock.NewFsWatcher(t)
	watcher.On("Add", "/home/user/Music").Return(nil).Once()

	backup := NewBackup(cfg, fs, watcher, nil, nil, nil)
	err := backup.InitFsWatcher()

	assert.ErrorIs(t, err, expErr)
}

func Test_Backup_InitFsWatcher_success(t *testing.T) {
	cfg := app.BackupConfig{
		WatchPaths: []string{
			"/home/user/.gitconfig",
			"/home/user/Music",
		},
	}

	fs := mock.NewFilesystem(t)
	fs.On("IsDir", "/home/user/.gitconfig").Return(false, nil).Once()
	fs.On("IsDir", "/home/user/Music").Return(true, nil).Once()
	fs.On("ReadDir", "/home/user/Music").Return([]pkg.DirEntry{{IsDir: true, Name: "Queen"}}, nil).Once()
	fs.On("ReadDir", "/home/user/Music/Queen").
		Return([]pkg.DirEntry{{IsDir: false, Name: "Bohemian Rhapsody.mp3"}}, nil).Once()

	watcher := mock.NewFsWatcher(t)
	watcher.On("Add", "/home/user/.gitconfig").Return(nil).Once()
	watcher.On("Add", "/home/user/Music").Return(nil).Once()
	watcher.On("Add", "/home/user/Music/Queen").Return(nil).Once()

	backup := NewBackup(cfg, fs, watcher, nil, nil, nil)
	err := backup.InitFsWatcher()

	assert.NoError(t, err)
}

func Test_Backup_Watch(t *testing.T) {
	cfg := app.BackupConfig{
		StripPathPrefix: "/src",
		BackupPath:      "/dst",
	}

	inEvents := []pkg.FsEvent{
		{
			Path: "/src/backup_dir_not_exists",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/src/check_backup_dir_error",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/src/backup_dir_is_a_file",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/src/create/is_dir_error",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/src/create/watch_and_sync_dir_errors",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/src/create/sync_file_error",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/src/create/sync_file_success",
			Op:   pkg.FsEventCreate,
		},
		{
			Path: "/src/remove/error",
			Op:   pkg.FsEventRemove,
		},
		{
			Path: "/src/remove/success",
			Op:   pkg.FsEventRemove,
		},
	}

	fs := mock.NewFilesystem(t)
	notify := mock.NewNotificationService(t)
	syncer := mock.NewSyncer(t)

	// /src/backup_dir_not_exists
	fs.On("IsDir", "/dst").Return(false, os.ErrNotExist).Once()
	notify.On(
		"PushWarningText",
		"Backup dir isn't found. Got event: dir=%s; op=%d",
		"/src/backup_dir_not_exists",
		pkg.FsEventCreate,
	).Once()
	// /src/check_backup_dir_error
	err0 := errors.New("error 0")
	fs.On("IsDir", "/dst").Return(false, err0).Once()
	notify.On("PushErr", err0).Once()
	notify.On(
		"PushWarningText",
		"Backup dir isn't found. Got event: dir=%s; op=%d",
		"/src/check_backup_dir_error",
		pkg.FsEventCreate,
	).Once()
	// /src/backup_dir_is_a_file
	fs.On("IsDir", "/dst").Return(false, nil).Once()
	notify.On("PushErrorText", "Is backup dir a file? O_o").Once()
	notify.On(
		"PushWarningText",
		"Backup dir isn't found. Got event: dir=%s; op=%d",
		"/src/backup_dir_is_a_file",
		pkg.FsEventCreate,
	).Once()
	// /src/create/is_dir_error
	err1 := errors.New("error 1")
	fs.On("IsDir", "/dst").Return(true, nil).Once()
	fs.On("IsDir", "/src/create/is_dir_error").Return(false, err1).Once()
	notify.On("PushErr", err1).Once()
	// /src/create/watch_and_sync_dir_errors
	err2 := errors.New("error 2")
	err3 := errors.New("error 3")
	fs.On("IsDir", "/dst").Return(true, nil).Once()
	fs.On("IsDir", "/src/create/watch_and_sync_dir_errors").Return(true, nil).Once()
	fs.On("ReadDir", "/src/create/watch_and_sync_dir_errors").Return(nil, err2).Once()
	fs.On("Mkdir", "/dst/create/watch_and_sync_dir_errors").Return(err3).Once()
	notify.On("PushErr", err2).Once()
	notify.On("PushErr", err3).Once()
	// /src/create/sync_file_error
	err4 := errors.New("error 4")
	fs.On("IsDir", "/dst").Return(true, nil).Once()
	fs.On("IsDir", "/src/create/sync_file_error").Return(false, nil).Once()
	fs.On("Mkdir", "/dst/create").Return(nil).Once()
	syncer.On("Sync", "/src/create/sync_file_error", "/dst/create", false).Return(err4).Once()
	notify.On("PushErr", err4).Once()
	// /src/create/sync_file_success
	fs.On("IsDir", "/dst").Return(true, nil).Once()
	fs.On("IsDir", "/src/create/sync_file_success").Return(false, nil).Once()
	fs.On("Mkdir", "/dst/create").Return(nil).Once()
	syncer.On("Sync", "/src/create/sync_file_success", "/dst/create", false).Return(nil).Once()
	notify.On("PushInfoText", "Synced: %s -> %s", "/src/create/sync_file_success", "/dst/create").Once()
	// /src/remove/error
	err5 := errors.New("error 5")
	fs.On("IsDir", "/dst").Return(true, nil).Once()
	fs.On("Remove", "/dst/remove/error").Return(err5).Once()
	notify.On("PushErr", err5).Once()
	// /src/remove/success
	fs.On("IsDir", "/dst").Return(true, nil).Once()
	fs.On("Remove", "/dst/remove/success").Return(nil).Once()
	notify.On("PushInfoText", "Deleted: %s", "/dst/remove/success").Once()

	events := make(chan pkg.FsEvent)
	backup := NewBackup(cfg, fs, nil, syncer, notify, events)
	backup.doneCh = make(chan pkg.FsEvent)
	go backup.Watch()

	for _, e := range inEvents {
		events <- e
		<-backup.doneCh // preserves order
	}

	backup.Close()
}
