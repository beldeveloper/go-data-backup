package errors

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_skipLines(t *testing.T) {
	tt := []struct {
		name   string
		str    string
		after  int
		n      int
		expStr string
	}{
		{
			name:   "empty string",
			str:    "",
			after:  0,
			n:      2,
			expStr: "",
		},
		{
			name:   "empty string, start from the line that not exists",
			str:    "",
			after:  2,
			n:      2,
			expStr: "",
		},
		{
			name:   "skip line in single line text",
			str:    "hello",
			after:  0,
			n:      1,
			expStr: "",
		},
		{
			name:   "skip nothing",
			str:    "hello\nthere",
			after:  0,
			n:      0,
			expStr: "hello\nthere",
		},
		{
			name:   "skip in the beginning",
			str:    "hello\nthere\nhow\nare\nyou",
			after:  0,
			n:      2,
			expStr: "how\nare\nyou",
		},
		{
			name:   "skip in the middle",
			str:    "hello\nthere\nhow\nare\nyou",
			after:  1,
			n:      3,
			expStr: "hello\nyou",
		},
		{
			name:   "skip all",
			str:    "hello\nthere\nhow\nare\nyou",
			after:  0,
			n:      5,
			expStr: "",
		},
		{
			name:   "skip more lines than exists",
			str:    "hello\nthere",
			after:  0,
			n:      4,
			expStr: "",
		},
		{
			name:   "start from the line that not exists",
			str:    "hello\nthere",
			after:  4,
			n:      4,
			expStr: "hello\nthere",
		},
	}

	for _, tc := range tt {
		str := skipLines(tc.str, tc.after, tc.n)

		assert.Equal(t, tc.expStr, str, tc.name)
	}
}
