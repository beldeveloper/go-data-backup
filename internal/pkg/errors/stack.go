package errors

import (
	"runtime/debug"
	"strings"
)

// GetStack returns a current stack trace.
func GetStack(skipItems int) string {
	stack := string(debug.Stack())
	// don't skip the first line because it contains a goroutine id
	// double lines to skip because each entry in a stack consists of two lines
	return skipLines(stack, 1, skipItems*2)
}

// skipLines removes n lines from a multiline string after a certain line.
func skipLines(s string, after, n int) string {
	lines := strings.Split(s, "\n")
	total := len(lines)

	if after > total {
		after = total
	}
	if after+n > total {
		n = total - after
	}

	lines = append(lines[:after], lines[after+n:]...)
	return strings.Join(lines, "\n")
}
