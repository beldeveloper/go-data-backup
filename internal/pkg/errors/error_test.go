package errors

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewErrorf(t *testing.T) {
	err := NewErrorf("test %s", "error")

	assert.Equal(t, "test error", err.Original.Error())
	assert.NotEmpty(t, err.Stack)
}

func Test_Wrap_already_wrapped(t *testing.T) {
	err := NewErrorf("test error")

	wrappedErr := Wrap(err)

	assert.Equal(t, err, wrappedErr)
}

func Test_Wrap_plain_error(t *testing.T) {
	err := Wrap(errors.New("test error"))

	assert.Equal(t, "test error", err.Original.Error())
	assert.NotEmpty(t, err.Stack)
}

func Test_Is(t *testing.T) {
	err1 := errors.New("error 1")
	err2 := errors.New("error 2")
	wrappedErr1 := Wrap(err1)
	wrappedErr2 := Wrap(err2)

	tt := []struct {
		name   string
		err    error
		target error
		expRes bool
	}{
		{
			name:   "plain errors match",
			err:    err1,
			target: err1,
			expRes: true,
		},
		{
			name:   "plain errors don't match",
			err:    err1,
			target: err2,
			expRes: false,
		},
		{
			name:   "wrapped errors match",
			err:    wrappedErr1,
			target: wrappedErr1,
			expRes: true,
		},
		{
			name:   "wrapped errors don't match",
			err:    wrappedErr1,
			target: wrappedErr2,
			expRes: false,
		},
		{
			name:   "wrapped error matches plain error",
			err:    wrappedErr1,
			target: err1,
			expRes: true,
		},
		{
			name:   "wrapped error don't match plain error",
			err:    wrappedErr1,
			target: err2,
			expRes: false,
		},
	}

	for _, tc := range tt {
		res := Is(tc.err, tc.target)

		assert.Equal(t, tc.expRes, res, tc.name)
	}
}

func Test_Error_returns_message(t *testing.T) {
	err := NewErrorf("test error").AddDetails(Details{"foo": "bar"})

	expMessage := "test error\nDetails:\nfoo=bar\nStack:\n"

	assert.Contains(t, err.Error(), expMessage)
}

func Test_Error_returns_message_with_no_original_error(t *testing.T) {
	err := Wrap(nil)

	expMessage := "<no original error>\nStack:\n"

	assert.Contains(t, err.Error(), expMessage)
}

func Test_AddDetails(t *testing.T) {
	details := Details{
		"foo": 1,
		"bar": "test",
	}

	err := NewErrorf("test error").AddDetails(details)

	assert.Equal(t, len(details), len(err.Details))
	for k, v := range details {
		assert.Equal(t, v, err.Details[k], k)
	}
}

func Test_Cast(t *testing.T) {
	err := NewErrorf("test error")

	tt := []struct {
		name     string
		err      error
		expError *Error
		expOk    bool
	}{
		{
			name:     "ok",
			err:      err,
			expError: err,
			expOk:    true,
		},
		{
			name:     "fail",
			err:      errors.New("test error"),
			expError: nil,
			expOk:    false,
		},
	}

	for _, tc := range tt {
		resError, resOk := Cast(tc.err)

		assert.Equal(t, tc.expError, resError)
		assert.Equal(t, tc.expOk, resOk)
	}
}
