package errors

import (
	"errors"
	"fmt"
)

type (
	Details map[string]any
)

func NewErrorf(msg string, v ...any) *Error {
	return &Error{
		Original: fmt.Errorf(msg, v...),
		Stack:    GetStack(3),
	}
}

func Wrap(err error) *Error {
	tErr, ok := err.(*Error)
	if ok {
		return tErr
	}

	return &Error{
		Original: err,
		Stack:    GetStack(3),
	}
}

func Is(err, target error) bool {
	tErr, ok := err.(*Error)
	if ok && errors.Is(tErr.Original, target) {
		return true
	}
	return errors.Is(err, target)
}

type Error struct {
	Original error
	Details  Details
	Stack    string
}

func (e *Error) Error() string {
	var msg string
	if e.Original != nil {
		msg = e.Original.Error()
	} else {
		msg = "<no original error>"
	}

	if len(e.Details) > 0 {
		msg += "\nDetails:"
		for k, v := range e.Details {
			msg += fmt.Sprintf("\n%s=%v", k, v)
		}
	}
	if e.Stack != "" {
		msg += "\nStack:\n" + e.Stack
	}

	return msg
}

func (e *Error) AddDetails(f Details) *Error {
	if e.Details == nil && len(f) > 0 {
		e.Details = make(Details, len(f))
	}
	for k, v := range f {
		e.Details[k] = v
	}
	return e
}

func Cast(err error) (tErr *Error, ok bool) {
	tErr, ok = err.(*Error)
	return
}
