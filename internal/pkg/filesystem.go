package pkg

type DirEntry struct {
	IsDir bool
	Name  string
}

//go:generate mockery --name Filesystem --outpkg mock --output ../../mock --filename filesystem.go
type Filesystem interface {
	IsDir(path string) (bool, error)
	ReadDir(path string) ([]DirEntry, error)
	Remove(path string) error
	Mkdir(path string) error
}
