package noop

import (
	"fmt"
	"log"
)

func NewNotificationService() NotificationService {
	return NotificationService{}
}

type NotificationService struct {
}

func (n NotificationService) PushInfoText(msg string, v ...any) {
	log.Println("ℹ️ " + fmt.Sprintf(msg, v...))
}

func (n NotificationService) PushWarningText(msg string, v ...any) {
	log.Println("⚠️ " + fmt.Sprintf(msg, v...))
}

func (n NotificationService) PushErrorText(msg string, v ...any) {
	log.Println("❌ " + fmt.Sprintf(msg, v...))
}

func (n NotificationService) PushErr(e error) {
	n.PushErrorText(e.Error())
}

func (n NotificationService) Stop() {
}
