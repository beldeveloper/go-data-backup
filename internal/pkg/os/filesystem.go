package os

import (
	"os"
	"strings"

	"go-data-backup/internal/pkg"
	"go-data-backup/internal/pkg/errors"
)

func NewFilesystem(jailDir string) Filesystem {
	return Filesystem{jailDir: jailDir}
}

type Filesystem struct {
	jailDir string
}

func (f Filesystem) IsDir(path string) (bool, error) {
	stat, err := os.Stat(path)
	if err != nil {
		return false, errors.Wrap(err).AddDetails(errors.Details{
			"path": path,
		})
	}
	return stat.IsDir(), nil
}

func (f Filesystem) ReadDir(path string) ([]pkg.DirEntry, error) {
	entries, err := os.ReadDir(path)
	if err != nil {
		return nil, errors.Wrap(err).AddDetails(errors.Details{
			"path": path,
		})
	}

	res := make([]pkg.DirEntry, 0, len(entries))
	for _, e := range entries {
		if e.Name() == "." || e.Name() == ".." {
			// just in case
			continue
		}

		res = append(res, pkg.DirEntry{
			Name:  e.Name(),
			IsDir: e.IsDir(),
		})
	}

	return res, nil
}

func (f Filesystem) Remove(path string) error {
	return f.jailOp(path, os.RemoveAll)
}

func (f Filesystem) Mkdir(path string) error {
	return f.jailOp(path, func(path string) error {
		return os.MkdirAll(path, os.ModePerm)
	})
}

func (f Filesystem) jailOp(path string, op func(string) error) error {
	if strings.Contains(path, "/../") || strings.HasPrefix(path, "../") || strings.HasSuffix(path, "/..") ||
		!strings.HasPrefix(path, f.jailDir) {
		return errors.NewErrorf("Unauthorized access").AddDetails(errors.Details{
			"path": path,
		})
	}

	err := op(path)
	if err != nil {
		return errors.Wrap(err).AddDetails(errors.Details{
			"path": path,
		})
	}

	return nil
}
