package telegram

import (
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	initialReconnectInterval = time.Second * 5
	requestTimeout           = time.Second * 5
	sendJobWaitInterval      = time.Second * 5
)

func NewNotificationService(token string, chatID int64) *NotificationService {
	return &NotificationService{token: token, chatID: chatID}
}

type NotificationService struct {
	token  string
	chatID int64
	bot    *tgbotapi.BotAPI

	queueMux sync.Mutex
	queue    []string

	stopMux sync.Mutex
	stopCh  chan int
}

func (n *NotificationService) PushInfoText(msg string, v ...any) {
	n.pushText("ℹ️ " + fmt.Sprintf(msg, v...))
}

func (n *NotificationService) PushWarningText(msg string, v ...any) {
	n.pushText("⚠️ " + fmt.Sprintf(msg, v...))
}

func (n *NotificationService) PushErrorText(msg string, v ...any) {
	n.pushText("❌ " + fmt.Sprintf(msg, v...))
}

func (n *NotificationService) PushErr(e error) {
	n.PushErrorText(e.Error())
}

func (n *NotificationService) pushText(t string) {
	log.Println(t)

	n.queueMux.Lock()
	defer n.queueMux.Unlock()
	n.queue = append(n.queue, t)
}

func (n *NotificationService) RunJob() {
	n.jobConnect()
	n.jobSend()
}

func (n *NotificationService) jobConnect() {
	httpClient := &http.Client{
		Timeout: requestTimeout,
	}

	var err error

	for {
		if n.isStopped() {
			n.reportStop()
			return
		}

		n.bot, err = tgbotapi.NewBotAPIWithClient(n.token, tgbotapi.APIEndpoint, httpClient)
		if err != nil {
			time.Sleep(initialReconnectInterval)
			continue
		}

		log.Println("Telegram bot connected")

		return
	}
}

func (n *NotificationService) jobSend() {
	for {
		if n.isStopped() {
			n.sendQueue()
			n.reportStop()
			return
		}
		n.sendQueue()
		time.Sleep(sendJobWaitInterval)
	}
}

func (n *NotificationService) sendQueue() {
	queue := n.stealQueue()
	if len(queue) == 0 {
		return
	}

	for i, t := range queue {
		err := n.sendText(t)
		if err != nil {
			n.reEnqueue(queue[i:])
			return
		}
	}
}

func (n *NotificationService) stealQueue() []string {
	n.queueMux.Lock()
	defer n.queueMux.Unlock()

	if len(n.queue) == 0 {
		return nil
	}

	q := n.queue
	n.queue = nil
	return q
}

func (n *NotificationService) reEnqueue(q []string) {
	n.queueMux.Lock()
	defer n.queueMux.Unlock()
	n.queue = append(q, n.queue...)
}

func (n *NotificationService) sendText(t string) error {
	msg := tgbotapi.NewMessage(n.chatID, t)
	_, err := n.bot.Send(msg)
	return err
}

func (n *NotificationService) Stop() {
	n.stopMux.Lock()
	n.stopCh = make(chan int)
	n.stopMux.Unlock()

	<-n.stopCh
}

func (n *NotificationService) isStopped() bool {
	n.stopMux.Lock()
	defer n.stopMux.Unlock()
	return n.stopCh != nil
}

func (n *NotificationService) reportStop() {
	close(n.stopCh)
}
