package pkg

//go:generate mockery --name Syncer --outpkg mock --output ../../mock --filename syncer.go
type Syncer interface {
	Sync(src, dst string, isDir bool) error
}
