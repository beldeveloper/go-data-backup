package pkg

//go:generate mockery --name NotificationService --outpkg mock --output ../../mock --filename notification.go
type NotificationService interface {
	PushInfoText(msg string, v ...any)
	PushWarningText(msg string, v ...any)
	PushErrorText(msg string, v ...any)
	PushErr(e error)
	Stop()
}
