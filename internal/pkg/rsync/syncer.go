package rsync

import (
	"bytes"
	"os/exec"
	"strings"

	"go-data-backup/internal/pkg/errors"
)

func NewSyncer() Syncer {
	return Syncer{}
}

type Syncer struct {
}

func (s Syncer) Sync(src, dst string, isDir bool) error {
	args := make([]string, 0, 3)

	if isDir {
		args = append(args, "-r")
		src += "/"
	}

	args = append(args, src, dst)

	osCmd := exec.Command("/usr/bin/rsync", args...)
	var stderr bytes.Buffer
	osCmd.Stderr = &stderr
	err := osCmd.Run()
	if err != nil {
		return errors.Wrap(err).AddDetails(errors.Details{
			"stderr": strings.TrimSpace(stderr.String()),
		})
	}

	return nil
}
