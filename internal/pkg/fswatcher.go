package pkg

const (
	FsEventCreate = iota
	FsEventModify
	FsEventRename
	FsEventRemove
)

type FsEvent struct {
	Path string
	Op   int
}

//go:generate mockery --name FsWatcher --outpkg mock --output ../../mock --filename fswatcher.go
type FsWatcher interface {
	Add(path string) error
	Events() <-chan FsEvent
}
