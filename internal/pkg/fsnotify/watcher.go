package fsnotify

import (
	"strings"

	"go-data-backup/internal/pkg"
	"go-data-backup/internal/pkg/errors"

	"github.com/fsnotify/fsnotify"
)

func NewWatcher(notify pkg.NotificationService) *Watcher {
	base, err := fsnotify.NewWatcher()
	if err != nil {
		notify.PushErr(err)
		return nil
	}
	return &Watcher{
		base:     base,
		notify:   notify,
		eventsCh: make(chan pkg.FsEvent),
	}
}

type Watcher struct {
	base   *fsnotify.Watcher
	notify pkg.NotificationService

	eventsCh chan pkg.FsEvent
}

func (w *Watcher) Add(path string) error {
	err := w.base.Add(path)
	if err != nil {
		return errors.Wrap(err).AddDetails(errors.Details{
			"path": path,
		})
	}
	return nil
}

func (w *Watcher) Events() <-chan pkg.FsEvent {
	return w.eventsCh
}

func (w *Watcher) Watch() {
	for {
		select {
		case event, ok := <-w.base.Events:
			if !ok {
				w.notify.PushWarningText("FS watcher stopped")
				return
			}
			if strings.HasSuffix(event.Name, "~") {
				continue
			}
			w.eventsCh <- w.castEvent(event)
		case err, ok := <-w.base.Errors:
			if !ok {
				w.notify.PushWarningText("FS watcher stopped")
				return
			}
			w.notify.PushErr(errors.Wrap(err))
		}
	}
}

func (w *Watcher) Close() {
	err := w.base.Close()
	if err != nil {
		w.notify.PushErr(err)
	}
	close(w.eventsCh)
}

func (w *Watcher) castEvent(event fsnotify.Event) (res pkg.FsEvent) {
	res.Path = event.Name
	switch event.Op {
	case fsnotify.Create:
		res.Op = pkg.FsEventCreate
	case fsnotify.Write:
		res.Op = pkg.FsEventModify
	case fsnotify.Rename:
		res.Op = pkg.FsEventRename
	case fsnotify.Remove:
		res.Op = pkg.FsEventRemove
	}
	return
}
